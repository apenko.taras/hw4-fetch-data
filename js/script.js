const URL = 'https://ajax.test-danit.com/api/swapi/films'

const  fetchCharacter = (url) => {
    return new Promise((resolve) => {
        fetch(url)
            .then(res => res.json())
            .then((data) => {
                resolve(data.name);
            });
    });
}


fetch(URL)
    .then(res => res.json())
    .then(data => {
        data.forEach(item => {
            let film = document.createElement("div");
            film.insertAdjacentHTML("beforeend",
                `<div><p>Name: ${item.name}, No: ${item.id} Descript: ${item.openingCrawl}</p></div>`)

            Promise.all(item.characters.map(item => fetchCharacter((item))))
                .then(heroes => film.insertAdjacentHTML("beforeend",
                    `<div><p>characters: ${heroes}.</p></div>`))

            document.body.insertAdjacentElement("beforeend", film)
        })
    })
